import yagmail
import argparse

def main(date,email):


    yag = yagmail.SMTP('remindme.techchallenge@gmail.com', 'techchallenge')
    contents = [
        'Lieber Patient, \n\n' +

        'hiermit ist ihr Termin am ' + date + ' bestätigt. ' +
        'Anbei finden Sie eine Checkliste, '
        'die Sie über alle weiteren Schritte informiert. ' +
        'Wir bitten Sie, die darin aufgeführten '
        'Anweisungen gründlich zu lesen und zu befolgen, um ' +
        'eine erfolgreiche Behandlung zu gewährleisten. '
        'Wir werden Sie über diesen Kontaktweg über etwaige ' +
        'Terminänderungen auf dem Laufenden halten und '
        'Sie gegebenenfalls an einzelne Schritte erinnern.\n\n' +

        'Das MRI Team wünscht Ihren alles Gute!\n\n' +

        'Ihre Checkliste: www.remindME-MRI.de/Operation-Diabetiker-Blutverdünner/sjkbdBCWIlmcewwW/n'
    ]
    yag.send('robin.geibel@gmx.net', 'Information zu Ihrer Operation am ' + date, contents)



if __name__ == "__main__":
    arg_parse =  argparse.ArgumentParser(description='mail.py')

    arg_parse.add_argument("-input_date", default= "2021-01-16",action="store",dest="input_date",type=str)
    arg_parse.add_argument("-email", action="store", default="robin.geibel@gmx.net" ,dest="email", type=str)

    arguments = arg_parse.parse_args()
    date = arguments.input_date
    email = arguments.email
    main(date,email)
